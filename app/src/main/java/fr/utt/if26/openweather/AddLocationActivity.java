package fr.utt.if26.openweather;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.aksingh.owmjapis.core.OWM;

import fr.utt.if26.openweather.helper.Database;
import fr.utt.if26.openweather.model.Location;
import fr.utt.if26.openweather.service.WeatherService;

public class AddLocationActivity extends AppCompatActivity implements View.OnClickListener {

    Database database;

    EditText city;
    EditText countryCode;

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        city = findViewById(R.id.activity_add_location_city);
        countryCode = findViewById(R.id.activity_add_location_country_code);

        button = findViewById(R.id.activity_add_location_button);
        button.setOnClickListener(this);

        database = new Database(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.activity_add_location_button) {
            OWM.Country country = WeatherService.findCountryByValue(countryCode.getText().toString().toUpperCase());
            if (country == null) {
                Toast.makeText(this, "Country code is incorrect", Toast.LENGTH_LONG).show();
                return;
            }

            Location location = new Location(city.getText().toString(), country);

            if (!WeatherService.isLocationExists(this, location)) {
                Toast.makeText(this, "L'emplacement n'existe pas", Toast.LENGTH_LONG).show();
                return;
            }

            database.open();
            long id = Database.mLocationDAO.addLocation(location);
            database.close();

            if (id != -1) {
                Intent intent = new Intent(this, NavigationDrawerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("location", location);
                startActivity(intent);
            } else {
                Toast.makeText(this, "L'emplacement n'a pas été enregistré", Toast.LENGTH_LONG).show();
            }
        }
    }
}
