package fr.utt.if26.openweather;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import fr.utt.if26.openweather.helper.Database;
import fr.utt.if26.openweather.model.Location;
import fr.utt.if26.openweather.settings.SettingsActivity;
import fr.utt.if26.openweather.settings.SettingsObservable;

public class NavigationDrawerActivity extends AppCompatActivity implements Observer, View.OnClickListener, AdapterView.OnItemClickListener {

    Database database;

    MainFragment mainFragment;

    Button addLocationButton;
    ListView navLocationListView;

    List<Location> locations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NoticeDialog.confirmRGPD(this);

        // Add observable
        NavigationDrawerActivityObservable.getInstance().addObserver(this);
        SettingsObservable.getInstance().addObserver(this);

        database = new Database(this);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navLocationListView = navigationView.findViewById(R.id.nav_list_view_locations);
        navLocationListView.setOnItemClickListener(this);
        addLocationButton = navigationView.findViewById(R.id.nav_add_location);
        addLocationButton.setOnClickListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainFragment != null)
                    mainFragment.updateView();
                Snackbar.make(view, "Weather update", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        updateNavLocationListView();

        updateFragmentView(null);
    }

    public void updateFragmentView(@Nullable Location location) {
        mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.container_fragment_main_weather);
        if (mainFragment != null) {
            if (location != null) {
                mainFragment.setLocation(location).updateView();
                return;
            }

            Location locationFromIntent = (Location) getIntent().getSerializableExtra("location");
            if (locationFromIntent != null) {
                mainFragment.setLocation(locationFromIntent).updateView();
                return;
            }

            if (this.locations != null && this.locations.size() > 0) {
                mainFragment.setLocation(this.locations.get(0)).updateView();
                return;
            }

            mainFragment.updateView();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment_main_weather, MainFragment.newInstance().setLocation(null))
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commitAllowingStateLoss()
            ;
        }
    }

    public void updateNavLocationListView() {
        database.open();

        this.locations = Database.mLocationDAO.fetchAllLocations();

        LocationNavAdapter locationNavAdapter = new LocationNavAdapter(this, R.layout.nav_location_item, locations);
        navLocationListView.setAdapter(locationNavAdapter);
        locationNavAdapter.notifyDataSetChanged();

        database.close();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_add_location:
                Intent intent = new Intent(this, AddLocationActivity.class);
                startActivity(intent);
                onBackPressed();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.nav_list_view_locations:
                Location location = (Location) parent.getItemAtPosition(position);
                updateFragmentView(location);
                onBackPressed();
                break;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof NavigationDrawerActivityObservable || o instanceof SettingsObservable) {
            updateNavLocationListView();
            updateFragmentView(null);
        }
    }

    public static class NavigationDrawerActivityObservable extends Observable {
        private static NavigationDrawerActivityObservable instance;

        public static NavigationDrawerActivityObservable getInstance() {
            if (instance == null)
                instance = new NavigationDrawerActivityObservable();

            return instance;
        }

        public void notifyObserversWithChanged(@Nullable Object arg) {
            setChanged();
            this.notifyObservers(arg);
        }
    }
}
