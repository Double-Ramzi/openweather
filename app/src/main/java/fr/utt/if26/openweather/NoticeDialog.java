package fr.utt.if26.openweather;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Date;

public class NoticeDialog {

    private static final String RGPD_AGREEMENT = "rgpd_agreement";
    private static final String RGPD_AGREEMENT_DATE = "rgpd_agreement_date";

    /**
     *
     * @param context - Context
     */
    public static void confirmRGPD(final Context context) {
        if (alreadyDidIt(context))
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(
                "Bienvenue sur OpenWeather !\n" +
                "On ne stocke pas votre géolocalisation et ni vos informations personnelles.\n" +
                "Les données météo proviennent de l'API OpenWeatherMap")
                .setPositiveButton("J'ai compris", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        setConfirmationFlag(context);
                    }
                })
        ;

        Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private static boolean alreadyDidIt(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getBoolean(RGPD_AGREEMENT, false);
    }

    private static void setConfirmationFlag(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit()
                .putBoolean(RGPD_AGREEMENT, true)
                .putLong(RGPD_AGREEMENT_DATE, new Date().getTime())
                .apply()
        ;
    }

}
