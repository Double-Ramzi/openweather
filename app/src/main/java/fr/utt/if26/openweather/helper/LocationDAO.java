package fr.utt.if26.openweather.helper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.utt.if26.openweather.model.Location;

public class LocationDAO extends DbContentProvider implements ILocationSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public LocationDAO(SQLiteDatabase db) {
        super(db);
    }

    public Location fetchLocationByID(int id) {
        final String selectionArgs[] = {String.valueOf(id)};
        final String selection = COLUMN_ID + " = ?";
        Location location = new Location();
        cursor = super.query(LOCATION_TABLE, LOCATION_COLUMNS, selection, selectionArgs, COLUMN_ID);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                location = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return location;
    }

    public List<Location> fetchAllLocations() {
        List<Location> locationList = new ArrayList<>();
        cursor = super.query(LOCATION_TABLE, LOCATION_COLUMNS, null, null, COLUMN_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Location location = cursorToEntity(cursor);
                locationList.add(location);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return locationList;
    }

    public long addLocation(Location location) {
        setContentValue(location);
        try {
            return super.insert(LOCATION_TABLE, getContentValue());
        } catch (SQLiteConstraintException ex) {
            Log.w("Database", ex.getMessage());
            return -1;
        }
    }

    public boolean deleteLocation(Location location) {
        final String selectionArgs[] = {String.valueOf(location.id)};
        final String selection = COLUMN_ID + " = ?";

        return super.delete(LOCATION_TABLE, selection, selectionArgs) > 0;
    }

    @Override
    protected Location cursorToEntity(Cursor cursor) {
        Location location = new Location();

        if (cursor != null) {
            if (cursor.getColumnIndex(COLUMN_ID) != -1) {
                int idIndex = cursor.getColumnIndexOrThrow(COLUMN_ID);
                location.id = cursor.getLong(idIndex);
            }
            if (cursor.getColumnIndex(COLUMN_CITY) != -1) {
                int cityIndex = cursor.getColumnIndexOrThrow(COLUMN_CITY);
                location.city = cursor.getString(cityIndex);
            }
            if (cursor.getColumnIndex(COLUMN_COUNTRY_CODE) != -1) {
                int countryCodeIndex = cursor.getColumnIndexOrThrow(COLUMN_COUNTRY_CODE);
                try {
                    location.setCountry(cursor.getString(countryCodeIndex));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return location;
    }

    private void setContentValue(Location location) {
        initialValues = new ContentValues();
        if (location.id != 0)
            initialValues.put(COLUMN_ID, location.id);
        initialValues.put(COLUMN_CITY, location.city);
        initialValues.put(COLUMN_COUNTRY_CODE, location.countryCode);
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

}