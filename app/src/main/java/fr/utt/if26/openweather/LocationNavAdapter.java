package fr.utt.if26.openweather;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.model.CurrentWeather;
import net.aksingh.owmjapis.model.param.Weather;

import java.util.List;

import fr.utt.if26.openweather.helper.Database;
import fr.utt.if26.openweather.model.Location;
import fr.utt.if26.openweather.service.WeatherService;

public class LocationNavAdapter extends ArrayAdapter<Location> {

    int resource;

    public LocationNavAdapter(@NonNull Context context, int resource, List<Location> locations) {
        super(context, resource, locations);
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(this.resource, parent, false);
        }

        ImageView cross = convertView.findViewById(R.id.nav_location_item_delete);

        ImageView icon = convertView.findViewById(R.id.nav_location_item_image);
        TextView temp = convertView.findViewById(R.id.nav_location_item_temperature);
        TextView city = convertView.findViewById(R.id.nav_location_item_city);
        TextView info = convertView.findViewById(R.id.nav_location_item_info);

        final Location location = getItem(position);

        if (location == null) {
            return convertView;
        }

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getContext().getString(R.string.location_delete))
                        .setMessage(getContext().getString(R.string.location_delete_question))
                        .setPositiveButton(getContext().getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Database database = new Database(getContext());
                                database.open();
                                boolean isDeleted = Database.mLocationDAO.deleteLocation(location);
                                if (isDeleted) {
                                    Toast.makeText(getContext(), location.city + " a été supprimée", Toast.LENGTH_LONG).show();
                                    NavigationDrawerActivity.NavigationDrawerActivityObservable.getInstance().notifyObserversWithChanged(null);
                                }
                                database.close();
                            }

                        })
                        .setNegativeButton(getContext().getString(R.string.dialog_no), null)
                        .show();
            }
        });

        CurrentWeather currentWeather = null;
        try {
            currentWeather = WeatherService.getOWMInstance(this.getContext()).currentWeatherByCityName(location.city, location.getCountry());
        } catch (APIException e) {
            e.printStackTrace();
        }

        if (currentWeather != null) {
            if (currentWeather.hasMainData() && currentWeather.getMainData().hasTemp()) {
                temp.setText(String.format("%s %s", (int) Math.round(currentWeather.getMainData().getTemp()), WeatherService.UNIT_TEMP));
            }

            if (currentWeather.hasCityName()) {
                city.setText(String.format("%s, %s\n", currentWeather.getCityName(), currentWeather.getSystemData().getCountryCode()));
            }

            if (currentWeather.hasWeatherList()) {
                for (Weather weather : currentWeather.getWeatherList()) {
                    info.setText(weather.getMoreInfo());
                    if (weather.hasIconLink()) {
                        Picasso.get().load(weather.getIconLink()).into(icon);
                    }
                }
            }
        }

        return convertView;
    }
}
