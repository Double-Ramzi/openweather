package fr.utt.if26.openweather;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.param.Weather;
import net.aksingh.owmjapis.model.param.WeatherData;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import fr.utt.if26.openweather.service.WeatherService;

public class WeatherDataAdapter extends ArrayAdapter<WeatherData> {

    private int resource;

    public WeatherDataAdapter(@NonNull Context context, int resource, List<WeatherData> weatherData) {
        super(context, resource, weatherData);
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(this.resource, parent, false);
        }

        TextView day = convertView.findViewById(R.id.daily_forecast_item_day);
        ImageView icon = convertView.findViewById(R.id.daily_forecast_item_icon);
        TextView temp = convertView.findViewById(R.id.daily_forecast_item_temp);

        WeatherData weatherData = getItem(position);

        if (weatherData.hasDateTime()) {
            OWM OWMInstance = WeatherService.getOWMInstance(getContext());
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE", Locale.forLanguageTag(OWMInstance.getLanguage().getValue()));
            day.setText(dateFormat.format(weatherData.getDateTime()).toUpperCase());
        }

        if (weatherData.hasWeatherList()) {
            for (Weather weather : weatherData.getWeatherList()) {
                if (weather.hasIconLink()) {
                    Picasso.get().load(weather.getIconLink()).into(icon);
                }
            }
        }

        if (weatherData.hasMainData()) {
            temp.setText(String.format("%s %s", (int) Math.round(weatherData.getMainData().getTemp()), WeatherService.UNIT_TEMP));
        }

        return convertView;
    }
}
