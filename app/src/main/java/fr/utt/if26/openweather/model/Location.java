package fr.utt.if26.openweather.model;

import net.aksingh.owmjapis.core.OWM;

import java.io.Serializable;

import fr.utt.if26.openweather.service.WeatherService;

public class Location implements Serializable {
    public long id;
    public String city;
    public String countryCode;

    public Location() {}

    public Location(String city, OWM.Country country) {
        this.city = city;
        this.countryCode = country.getValue();
    }

    public Location setCountry(String countryCode) throws Exception {
        OWM.Country country = WeatherService.findCountryByValue(countryCode);
        if (country == null) {
            throw new Exception(countryCode + " doesn't exists !");
        }
        this.countryCode = country.getValue();
        return this;
    }

    public OWM.Country getCountry() {
        return WeatherService.findCountryByValue(this.countryCode);
    }
}
