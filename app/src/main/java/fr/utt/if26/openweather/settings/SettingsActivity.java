package fr.utt.if26.openweather.settings;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import net.aksingh.owmjapis.core.OWM;

import java.util.ArrayList;
import java.util.List;

import fr.utt.if26.openweather.R;
import fr.utt.if26.openweather.service.WeatherService;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {

    public static final String SETTINGS_KEY_API_UNIT = "api_unit";
    public static final String SETTINGS_KEY_API_LANGUAGE = "api_language";

    public static void bindSummaryToValue(Preference preference, Object newValue) {
        String value = newValue.toString();

        if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(newValue.toString());

            // Set the summary to reflect the new value.
            preference.setSummary(
                    index >= 0
                            ? listPreference.getEntries()[index]
                            : null);
        } else {
            preference.setSummary(value);
        }
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            final ListPreference listPreferenceUnit = (ListPreference) findPreference(SETTINGS_KEY_API_UNIT);
            setUnitListPreferenceData(listPreferenceUnit);
            bindPreferenceOnChange(listPreferenceUnit);

            final ListPreference listPreferenceLanguage = (ListPreference) findPreference(SETTINGS_KEY_API_LANGUAGE);
            setLanguageListPreferenceData(listPreferenceLanguage);
            bindPreferenceOnChange(listPreferenceLanguage);

            PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    if (key.equals(SETTINGS_KEY_API_UNIT) || key.equals(SETTINGS_KEY_API_LANGUAGE)) {
                        // Update Observers
                        SettingsObservable.getInstance().notifyObserversWithChanged(getActivity());
                    }
                }
            });
        }

        protected void bindPreferenceOnChange(Preference preference) {
            preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    bindSummaryToValue(preference, newValue);
                    return true;
                }
            });
        }

        protected static void setLanguageListPreferenceData(ListPreference listPreference) {
            List<CharSequence> entries = new ArrayList<>();
            List<CharSequence> entryValues = new ArrayList<>();

            for (OWM.Language language : OWM.Language.values()) {
                entries.add(language.getValue());
                entryValues.add(language.getValue());
            }

            listPreference.setEntries(entries.toArray(new CharSequence[entries.size()]));
            if (listPreference.getValue() != null)
                listPreference.setDefaultValue(WeatherService.defaultLanguage);
            listPreference.setSummary(listPreference.getValue());
            listPreference.setEntryValues(entryValues.toArray(new CharSequence[entryValues.size()]));
        }

        protected static void setUnitListPreferenceData(ListPreference listPreference) {
            List<CharSequence> entries = new ArrayList<>();
            List<CharSequence> entryValues = new ArrayList<>();

            for (OWM.Unit unit : OWM.Unit.values()) {
                entries.add(unit.getValue());
                entryValues.add(unit.getValue());
            }

            listPreference.setEntries(entries.toArray(new CharSequence[entries.size()]));
            if (listPreference.getValue() != null)
                listPreference.setDefaultValue(WeatherService.defaultUnit);
            listPreference.setSummary(listPreference.getValue());
            listPreference.setEntryValues(entryValues.toArray(new CharSequence[entryValues.size()]));
        }
    }

}
