package fr.utt.if26.openweather;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import net.aksingh.owmjapis.model.HourlyWeatherForecast;
import net.aksingh.owmjapis.model.param.Weather;

import fr.utt.if26.openweather.model.Location;
import fr.utt.if26.openweather.service.WeatherService;


public class MainFragment extends Fragment {
    private static final String ARG_CITY = "city";

    private Location location;
    private String city;

    private LinearLayout rootLayout;

    public ImageView imageView;

    public TextView textViewMainCity;
    public TextView textViewMainTemp;
    public TextView textViewDesc;

    public TextView textViewPressure;
    public TextView textViewHumidity;
    public TextView textViewWind;

    public ImageView imagePressure;
    public ImageView imageHumidity;
    public ImageView imageWind;

    public TextView textForecastNextDays;

    public ListView listView;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainFragment.
     */
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_CITY, city);
        fragment.setArguments(args);
        return fragment;
    }

    public MainFragment setLocation(@Nullable Location location) {
        this.location = location;
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeatherService.getOWMInstance(getString(R.string.owm_api_key), getActivity());

        if (getArguments() != null) {
            city = getArguments().getString(ARG_CITY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        rootLayout = view.findViewById(R.id.fragment_main_layout);

        imageView = view.findViewById(R.id.activity_main_image);

        textViewMainCity = view.findViewById(R.id.activity_main_city);
        textViewMainTemp = view.findViewById(R.id.activity_main_temp);
        textViewDesc = view.findViewById(R.id.activity_main_description);

        textViewPressure = view.findViewById(R.id.activity_main_pressure);
        textViewHumidity = view.findViewById(R.id.activity_main_humidity);
        textViewWind = view.findViewById(R.id.activity_main_wind);

        imagePressure = view.findViewById(R.id.image_pressure);
        imageHumidity = view.findViewById(R.id.image_humidity);
        imageWind = view.findViewById(R.id.image_wind);

        textForecastNextDays = view.findViewById(R.id.forecast_next_days);

        listView = view.findViewById(R.id.activity_main_list_view);
    }

    public void updateView() {
        if (this.location == null) {
            textViewMainCity.setText("Aucune ville trouvée\n\nVeuillez ajouter une ville dans\n Menu > Ajouter un emplacement");
            imagePressure.setVisibility(View.INVISIBLE);
            imageHumidity.setVisibility(View.INVISIBLE);
            imageWind.setVisibility(View.INVISIBLE);
            textForecastNextDays.setVisibility(View.INVISIBLE);
            return;
        }

        OWM owm = WeatherService.getOWMInstance(getActivity());

        CurrentWeather cwd = null;
        HourlyWeatherForecast hwf = null;
        try {
            cwd = owm.currentWeatherByCityName(this.location.city, this.location.getCountry());
            hwf = owm.hourlyWeatherForecastByCityName(this.location.city, this.location.getCountry());
        } catch (APIException e) {
            Toast.makeText(getActivity(), "APIException code : " + e.getInfo(), Toast.LENGTH_LONG).show();
        }

        imagePressure.setVisibility(View.VISIBLE);
        imageHumidity.setVisibility(View.VISIBLE);
        imageWind.setVisibility(View.VISIBLE);
        textForecastNextDays.setVisibility(View.VISIBLE);

        // checking data retrieval was successful or not
        if (cwd != null && cwd.hasRespCode() && cwd.getRespCode() == 200) {

            Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.ic_launcher_background);
            imageView.setImageDrawable(drawable);

            if (cwd.hasCityName()) {
                textViewMainCity.setText(String.format("%s, %s\n", cwd.getCityName(), cwd.getSystemData().getCountryCode()));
            }

            if (cwd.hasMainData() && cwd.getMainData().hasTemp()) {
                textViewMainTemp.setText(String.format("%s %s", (int) Math.round(cwd.getMainData().getTemp()), WeatherService.UNIT_TEMP));
            }

            if (cwd.hasWeatherList()) {
                for (Weather weather : cwd.getWeatherList()) {
                    textViewDesc.setText(weather.getMoreInfo());
                    if (weather.hasIconLink()) {
                        Picasso.get().load(weather.getIconLink()).into(imageView);
                    }
                }
            }

            if (cwd.hasMainData() && cwd.getMainData() != null) {
                if (cwd.getMainData().hasPressure())
                    textViewPressure.setText(String.format("%s %s", (int) Math.round(cwd.getMainData().getPressure()), WeatherService.UNIT_PRESSURE));
                if (cwd.getMainData().hasHumidity())
                    textViewHumidity.setText(String.format("%s %s", (int) Math.round(cwd.getMainData().getHumidity()), WeatherService.UNIT_HUMIDITY));
            }

            if (cwd.hasWindData() && cwd.getWindData() != null) {
                if (cwd.getWindData().hasSpeed())
                    textViewWind.setText(String.format("%s %s", cwd.getWindData().getSpeed(), WeatherService.UNIT_WIND));
            }
        }

        if (hwf != null && hwf.hasDataList()) {
            WeatherDataAdapter weatherAdapter = new WeatherDataAdapter(getContext(), R.layout.daily_weather_item, hwf.getDataList());
            listView.setAdapter(weatherAdapter);
        }
    }

}
