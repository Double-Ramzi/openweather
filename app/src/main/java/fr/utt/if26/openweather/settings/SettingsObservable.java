package fr.utt.if26.openweather.settings;

import java.util.Observable;

import fr.utt.if26.openweather.service.WeatherService;

public class SettingsObservable extends Observable {

    private static SettingsObservable instance;

    public static SettingsObservable getInstance() {
        if (instance == null)
            instance = new SettingsObservable();

        return instance;
    }

    public void notifyObserversWithChanged(Object arg) {
        setChanged();
        this.notifyObservers(arg);
    }

}
