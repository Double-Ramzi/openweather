package fr.utt.if26.openweather.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;

import java.util.Observable;
import java.util.Observer;

import fr.utt.if26.openweather.R;
import fr.utt.if26.openweather.model.Location;
import fr.utt.if26.openweather.settings.SettingsActivity;
import fr.utt.if26.openweather.settings.SettingsObservable;

public class WeatherService implements Observer {

    // Unit string definition
    public static String UNIT_TEMP = "°C";
    public static String UNIT_PRESSURE = "hpa";
    public static String UNIT_HUMIDITY = "%";
    public static String UNIT_WIND = "m/s";

    // Default preferences
    public static final OWM.Unit defaultUnit = OWM.Unit.METRIC;
    public static final OWM.Language defaultLanguage = OWM.Language.FRENCH;

    private static OWM OWMInstance;
    private static String apiKey;

    /**
     *
     * Get OWM unique instance
     *
     * @return OWM
     */
    public static OWM getOWMInstance(@Nullable Context context) {
        disableAndroidGuardNetwork();

        if (OWMInstance == null) {
            OWMInstance = new OWM(WeatherService.apiKey);

            // Add Observable
            SettingsObservable.getInstance().addObserver(new WeatherService());

            updateOWMPreferences(context);
        }

        return OWMInstance;
    }

    /**
     *
     * Instantiate or change API key of OWM
     *
     * @param apiKey - OWM API key
     * @param context - Context
     * @return OWM
     */
    public static OWM getOWMInstance(@NonNull String apiKey, @Nullable Context context) {
        WeatherService.apiKey = apiKey;

        return getOWMInstance(context);
    }

    /**
     * To see why
     */
    private static void disableAndroidGuardNetwork() {
        // Disable Android Guard Network
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().build();
        StrictMode.setThreadPolicy(policy);
    }

    /**
     * Update Language and Unit of OWM API depending on user's preferences
     * @param context - Context required to get user's preferences
     */
    private static void updateOWMPreferences(Context context) {
        //Default values
        OWM.Language language = defaultLanguage;
        OWM.Unit unit = defaultUnit;

        if (context != null) {
            language = getLanguagePreference(context);
            unit = getUnitPreference(context);

            updateUnitString(context);

            Toast.makeText(context, "Update OWM preferences " + language.getValue() + " " + unit.getValue(), Toast.LENGTH_LONG).show();
        }

        // Important set context to null otherwise cyclic call between getOWMInstance and updateOWMPreferences
        OWM instance = getOWMInstance(null);
        instance.setLanguage(language);
        instance.setUnit(unit);
    }

    private static void updateUnitString(@NonNull Context context) {
        UNIT_PRESSURE = context.getString(R.string.unit_pressure_hpa);
        UNIT_HUMIDITY = context.getString(R.string.unit_humidity_percentage);
        UNIT_WIND = context.getString(R.string.unit_wind_speed_meter_per_sec);

        OWM.Unit unit = getUnitPreference(context);
        if (unit == OWM.Unit.METRIC) {
            UNIT_TEMP = context.getString(R.string.unit_temperature_celsius);
        } else if (unit == OWM.Unit.IMPERIAL) {
            UNIT_TEMP = context.getString(R.string.unit_temperature_fahrenheit);
        } else {
            UNIT_TEMP = context.getString(R.string.unit_temperature_kelvin);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof SettingsObservable && arg instanceof Context) {
            updateOWMPreferences((Context) arg);
        }
    }

    /**
     * @param value - OWM.Unit value (ex: 'metric')
     * @return OWM.Unit Type
     */
    public static OWM.Unit findUnitByValue(final String value){
        for (OWM.Unit unit: OWM.Unit.values()) {
            if (unit.getValue().equals(value))
                return unit;
        }

        return defaultUnit;
    }

    /**
     *
     * Get Unit user's preference
     *
     * @param context - Context
     * @return OWM.Unit
     */
    public static OWM.Unit getUnitPreference(@NonNull Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return findUnitByValue(sharedPreferences.getString(SettingsActivity.SETTINGS_KEY_API_UNIT, defaultUnit.getValue()));
    }

    /**
     * @param value - OWM.Language value (ex: 'fr')
     * @return OWM.Language Type
     */
    public static OWM.Language findLanguageByValue(final String value) {
        for (OWM.Language language: OWM.Language.values()) {
            if (language.getValue().equals(value))
                return language;
        }

        return defaultLanguage;
    }

    /**
     *
     * Get Language user's preference
     *
     * @param context - Context
     * @return OWM.Language
     */
    public static OWM.Language getLanguagePreference(@NonNull Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return findLanguageByValue(sharedPreferences.getString(SettingsActivity.SETTINGS_KEY_API_LANGUAGE, defaultLanguage.getValue()));
    }

    public static OWM.Country findCountryByValue(final String value) {
        for (OWM.Country country: OWM.Country.values()) {
            if (country.getValue().equals(value))
                return country;
        }

        return null;
    }

    /**
     *
     * Return true if location exists, false otherwise
     *
     * @param context - Context
     * @param location - Location
     *
     * @return boolean
     */
    public static boolean isLocationExists(@NonNull Context context, Location location) {
        CurrentWeather currentWeather = null;
        try {
            OWM.Country country = findCountryByValue(location.countryCode);
            currentWeather = getOWMInstance(context).currentWeatherByCityName(location.city, country);
        } catch (APIException e) {
            e.printStackTrace();
        }

        return currentWeather != null;
    }
}
