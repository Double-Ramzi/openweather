package fr.utt.if26.openweather.helper;


public interface ILocationSchema {
    String LOCATION_TABLE = "location";
    String COLUMN_ID = "id";
    String COLUMN_CITY = "city";
    String COLUMN_COUNTRY_CODE = "country_code";
    String LOCATION_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + LOCATION_TABLE
            + " ("
            + COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_CITY
            + " VARCHAR(255) NOT NULL,"
            + COLUMN_COUNTRY_CODE
            + " VARCHAR(2) NOT NULL"
            + ")";

    String[] LOCATION_COLUMNS = new String[] {
            COLUMN_ID,
            COLUMN_CITY,
            COLUMN_COUNTRY_CODE
    };
}
